#!/usr/bin/env python
import os, sys
import math
from numpy import loadtxt
import matplotlib.pyplot as plt

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

########################################################
plt.figure(1);
plt.ylabel(" Discharge (m$^3$/s)", fontsize=16); 
plt.xlabel(" Time (days) ", fontsize=16); 
plt.title(" Discharge history at outlet ", fontsize=16)
#change the fontsize of minor ticks label 
plt.rc('xtick',labelsize=14)
plt.rc('ytick',labelsize=14)
plt.grid()

#plot Q
fileName = "postProcessing/probes/0/Q"
Q=[]
time=[]
abc =loadtxt(fileName, skiprows=3)
for z in abc:
	time.append(z[0]/86400)  #in hours
	Q.append(z[1])

plt.plot(time,Q,'r',label="Discharge at outlet")

#plt.xlim([0,5])
#plt.ylim([0,10])

plt.legend(loc='lower right')

plt.savefig("Q_outlet.png")
plt.show()

########################################################
#plot S
plt.figure(2);
plt.ylabel(" Conduit size (m$^2$)", fontsize=16);
plt.xlabel(" Time (days) ", fontsize=16);
plt.title(" Conduit size history at outlet ", fontsize=16)
#change the fontsize of minor ticks label 
plt.rc('xtick',labelsize=14)
plt.rc('ytick',labelsize=14)
plt.grid()

fileName = "postProcessing/probes/0/S"
S=[]
time=[]
abc =loadtxt(fileName, skiprows=3)
for z in abc:
    time.append(z[0]/86400)  #in hours
    S.append(z[1])

plt.plot(time,S,'r',label="Conduit size at outlet")

#plt.xlim([0,5])
#plt.ylim([0,10])

plt.legend(loc='lower right')

plt.savefig("S_outlet.png")
plt.show()

