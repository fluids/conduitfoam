1. To run the case, run ./Allrun
2. To clean the case, run ./Allclean
3. To change model parameters, edit the transportProperties file in constant
4. To change boundary conditions: edit files in 0
5. Pressure boundary condition for pressure at the outlet, go to the system/funkySetBoundaryDict
6. Pressure initial condition is set in the system/funkySetFieldsDict
