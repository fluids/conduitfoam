/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011-2017 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "lakeInletPressureFvPatchScalarField.H"
#include "addToRunTimeSelectionTable.H"
#include "volFields.H"
#include "one.H"

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::lakeInletPressureFvPatchScalarField::
lakeInletPressureFvPatchScalarField
(
    const fvPatch& p,
    const DimensionedField<scalar, volMesh>& iF
)
:
    fixedValueFvPatchField<scalar>(p, iF),
    inflowRate_(),
    lakeArea_(0.0),
    UName_("U"),
    SName_("S"),
    lakeVolume_(0.0),
    iceHeight_(0.0),
    headType_("discharge")
{}


Foam::lakeInletPressureFvPatchScalarField::
lakeInletPressureFvPatchScalarField
(
    const fvPatch& p,
    const DimensionedField<scalar, volMesh>& iF,
    const dictionary& dict
)
:
    fixedValueFvPatchField<scalar>(p, iF, dict, false),
    inflowRate_(Function1<scalar>::New("inflowRate", dict)),
    lakeArea_(readScalar(dict.lookup("lakeArea"))),
    UName_(dict.lookupOrDefault<word>("U", "U")),
    SName_(dict.lookupOrDefault<word>("S", "S")),
    lakeVolume_(readScalar(dict.lookup("lakeVolume"))),
    iceHeight_(readScalar(dict.lookup("iceHeight"))),
    headType_(dict.lookupOrDefault<word>("headType", "discharge"))

{
    if (dict.found("value"))
    {
        fvPatchField<scalar>::operator=
        (
            scalarField("value", dict, p.size())
        );
    }
}


Foam::lakeInletPressureFvPatchScalarField::
lakeInletPressureFvPatchScalarField
(
    const lakeInletPressureFvPatchScalarField& ptf,
    const fvPatch& p,
    const DimensionedField<scalar, volMesh>& iF,
    const fvPatchFieldMapper& mapper
)
:
    fixedValueFvPatchField<scalar>(ptf, p, iF, mapper),
    inflowRate_(ptf.inflowRate_, false),
    lakeArea_(ptf.lakeArea_),
    UName_(ptf.UName_),
    SName_(ptf.SName_),
    lakeVolume_(ptf.lakeVolume_),
    iceHeight_(ptf.iceHeight_),
    headType_(ptf.headType_)
{}


Foam::lakeInletPressureFvPatchScalarField::
lakeInletPressureFvPatchScalarField
(
    const lakeInletPressureFvPatchScalarField& ptf
)
:
    fixedValueFvPatchField<scalar>(ptf),
    inflowRate_(ptf.inflowRate_, false),
    lakeArea_(ptf.lakeArea_),
    UName_(ptf.UName_),
    SName_(ptf.SName_),
    lakeVolume_(ptf.lakeVolume_),
    iceHeight_(ptf.iceHeight_),
    headType_(ptf.headType_)
{}


Foam::lakeInletPressureFvPatchScalarField::
lakeInletPressureFvPatchScalarField
(
    const lakeInletPressureFvPatchScalarField& ptf,
    const DimensionedField<scalar, volMesh>& iF
)
:
    fixedValueFvPatchField<scalar>(ptf, iF),
    inflowRate_(ptf.inflowRate_, false),
    lakeArea_(ptf.lakeArea_),
    UName_(ptf.UName_),
    SName_(ptf.SName_),
    lakeVolume_(ptf.lakeVolume_),
    iceHeight_(ptf.iceHeight_),
    headType_(ptf.headType_)
{}


// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

void Foam::lakeInletPressureFvPatchScalarField::updateCoeffs()
{
    if (updated())
    {
        return;
    }

    //mean velocity in the conduit at the patch
    const fvPatchVectorField& Up =
        patch().lookupPatchField<volVectorField, vector>(UName_);
    
    //conduit cross-sectional area at the patch
    const fvPatchScalarField& Sp =
        patch().lookupPatchField<volScalarField, scalar>(SName_);

    //This BC can only be used in 1D case.
    if(Up.size() > 1)
    {
        FatalErrorInFunction 
            << "In correct case dimension. Only 1D case is supported with this BC. "
            << exit(FatalError);
    }

    //we also assume the 1D dimension is in the x direction. 
    scalar Qout = Up[0].x()*Sp[0];

    //time step size
    const scalar dt = db().time().deltaTValue();

    //solve the mass balance equation for the lake
    scalar Qin = inflowRate_->value(this->db().time().timeOutputValue());
    lakeVolume_ = lakeVolume_ + dt*(Qin - Qout);

    lakeVolume_ = max(0,min(lakeVolume_,lakeArea_*iceHeight_));
    
    /*if ((lakeVolume_ + dt*(Qin - Qout))<0)
    {
    lakeVolume_ =0;
    }
    else
    {
    lakeVolume_ = lakeVolume_ + dt*(Qin - Qout);
    }*/

    //water depth in the lake
    scalar h = lakeVolume_/lakeArea_;
    
    // If headType is specified by fiexed, using fixed h = iceHeight_.
    if (headType_ == "head") 
    {
      h = iceHeight_;
      if (Qout>=Qin) 
      {
      Info << "The required input flow rate is larger than given maximum." << endl;
      Qout = pow(-1.0,0.5);   // Call abort.
      }
    }

    //if (lakeVolume_ == 0) 
    //{
    //  Info << "The lake has been drained out." << endl;
    //  Qout = pow(-1.0,0.5);
    //}

    //water pressure/rhow
    scalar pw = 9.81*h;

    //write to log for postprocessing
    Info << "lakeInletPressure: Qin = " << Qin 
         << ", Qe = " << Qout 
         << ", he = " << h 
         << ", Ue = " << Up[0].x()
         << ", Se = " << Sp[0]
         << ", hi = " << iceHeight_
         << ", HType = " << headType_
         << endl;

    fvPatchField<scalar>::operator==(pw);

    fixedValueFvPatchScalarField::updateCoeffs();
}


void Foam::lakeInletPressureFvPatchScalarField::write(Ostream& os) const
{
    fvPatchField<scalar>::write(os);
    inflowRate_->writeData(os);
    os.writeKeyword("lakeArea") << lakeArea_ << token::END_STATEMENT << nl;
    writeEntryIfDifferent<word>(os, "U", "U", UName_);
    writeEntryIfDifferent<word>(os, "S", "S", SName_);
    writeEntryIfDifferent<word>(os, "headType", "Q", headType_);
    os.writeKeyword("lakeVolume") << lakeVolume_ << token::END_STATEMENT << nl;
    os.writeKeyword("iceHeight") << iceHeight_ << token::END_STATEMENT << nl;
    writeEntry("value", os);
}


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{
   makePatchTypeField
   (
       fvPatchScalarField,
       lakeInletPressureFvPatchScalarField
   );
}


// ************************************************************************* //
