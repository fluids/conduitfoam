# README #

# Subglacial conduit dynamics model implementation with OpenFOAM #

This is a repo for the subglacialConduitFoam model development and application.

### Relevant information ###

* OpenFOAM version: 5.x
* Requires funkySetFields and FunSetBounaryFields to set initial pressure field. 
  Download the swak4Foam and then run ./Allwmake to install funkeSetFields and FunSetBounaryFields

### #########################

Yunxiang Chen, Ph.D.
Department of Civil and Environmental Engineering
Penn State University
406 Sackett Building, University Park, PA 16802
Email: cyxcfd@gmail.com

Xiaofeng Liu, Ph.D., P.E.
Assistant Professor
Department of Civil and Environmental Engineering
Institute of CyberScience
Penn State University
223B Sackett Building, University Park, PA 16802
Tel: 814-863-2940 (O)
Web: http://water.engr.psu.edu/liu/
